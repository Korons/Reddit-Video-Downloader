# Reddit-Video-Downloader
A bash script thats uses Youtube-dl to download videos from user specified sites from subreddits 

Usage

Fill in VIDEO_DIR= with the directory where you want the videos to be stored

Fill in FILE_MIN= With the min file size you want

Fill in url_file= with a text file with a list of the subreddits you want videos from

Fill in sites_file= with a text file with a list of the sites you want videos from
