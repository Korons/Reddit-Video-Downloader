#!/bin/bash
start=$(date +%Y_%m_%d_%T)
#video_down.sh
# The dir were you want the videos
VIDEO_DIR=
# The min file size ex 50m
FILE_MIN=
# url_file is the config file with the url of the subreddits you want to download from. example www.reddit.com/r/mycoolvids/
url_file=
# sites_file is the file with site you want video links from
sites_file=
echo "video_down starting at $start" >> ~/script_log
while read line;
	do
	 	curl "$line" >> /tmp/video_links.txt
	done < $url_file
egrep  -o "\"url\": \"(http(s)?://){1}[^'\"]+" /tmp/video_links.txt >> /tmp/video_links2.txt 
while read line;
	do
		grep "$line.json" /tmp/video_links2.txt >> /tmp/video_links3.txt
	done < $sites_file

sed -i 's/"url": "/ /g' /tmp/video_links3.txt
sort /tmp/video_links3.txt | uniq > /tmp/video_links4.txt
cd "$VIDEO_DIR"
youtube-dl --min-filesize "$FILE_MIN" -a /tmp/video_links4.txt
cd /tmp
rm video_links*
fin=$(date +%Y_%m_%d_%T)
echo "video_down done at $fin" >> ~/script_log
